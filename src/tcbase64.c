
/*
        The little Base64, TCBASE4.C 

        clang tcbase64.c -o tcbase64
	(Fork of base64.c - by Joe DF (joedf@ahkscript.org)

	Released under the MIT License)
	Free Software Fundation


     Portable source code, which allows to  
     encode, using a method alike base64, of messages (emails,...). 
     The code is compilable with a C compiler, like clang, tcc, gcc,...
*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Base64 char table function - used internally for decoding
unsigned int b64_int(unsigned int ch);

// in_size : the number bytes to be encoded.
// Returns the recommended memory size to be allocated for the output buffer excluding the null byte
unsigned int b64e_size(unsigned int in_size);

// in_size : the number bytes to be decoded.
// Returns the recommended memory size to be allocated for the output buffer
unsigned int b64d_size(unsigned int in_size);

// in : buffer of "raw" binary to be encoded.
// in_len : number of bytes to be encoded.
// out : pointer to buffer with enough memory, user is responsible for memory allocation, receives null-terminated string
// returns size of output including null byte
unsigned int b64_encode(const unsigned char* in, unsigned int in_len, unsigned char* out);

// in : buffer of base64 string to be decoded.
// in_len : number of bytes to be decoded.
// out : pointer to buffer with enough memory, user is responsible for memory allocation, receives "raw" binary
// returns size of output excluding null byte
unsigned int b64_decode(const unsigned char* in, unsigned int in_len, unsigned char* out);

// file-version b64_encode
// Input : filenames
// returns size of output
unsigned int b64_encodef(char *InFile, char *OutFile);

// file-version b64_decode
// Input : filenames
// returns size of output
unsigned int b64_decodef(char *InFile, char *OutFile);

int cutelower(int a);

//Base64 char table - used internally for encoding
unsigned char b64_chr[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

unsigned int b64_int(unsigned int ch) {

	// ASCII to base64_int
	// 65-90  Upper Case  >>  0-25
	// 97-122 Lower Case  >>  26-51
	// 48-57  Numbers     >>  52-61
	// 43     Plus (+)    >>  62
	// 47     Slash (/)   >>  63
	// 61     Equal (=)   >>  64~
	if (ch==43)
	return 62;
	if (ch==47)
	return 63;
	if (ch==61)
	return 64;
	if ((ch>47) && (ch<58))
	return ch + 4;
	if ((ch>64) && (ch<91))
	return ch - 'A';
	if ((ch>96) && (ch<123))
	return (ch - 'a') + 26;
	return 0;
}


unsigned int b64e_size(unsigned int in_size) {

	// size equals 4*floor((1/3)*(in_size+2));
	int i, j = 0;
	for (i=0;i<in_size;i++) {
		if (i % 3 == 0)
		j += 1;
	}
	return (4*j);
}

unsigned int b64d_size(unsigned int in_size) {

	return ((3*in_size)/4);
}













unsigned int b64_encode( const unsigned char* in, unsigned int in_len, unsigned char* out) 
{
	unsigned int i=0, j=0, k=0, s[3];
	for (i=0;i<in_len;i++) {
		s[j++]=*(in+i);
		if (j==3) {
			out[k+0] = b64_chr[ (s[0]&255)>>2 ];
			out[k+1] = b64_chr[ ((s[0]&0x03)<<4)+((s[1]&0xF0)>>4) ];
			out[k+2] = b64_chr[ ((s[1]&0x0F)<<2)+((s[2]&0xC0)>>6) ];
			out[k+3] = b64_chr[ s[2]&0x3F ];
			j=0; k+=4;
		}
	}
	
	if (j) {
		if (j==1)
			s[1] = 0;
		out[k+0] = b64_chr[ (s[0]&255)>>2 ];
		out[k+1] = b64_chr[ ((s[0]&0x03)<<4)+((s[1]&0xF0)>>4) ];
		if (j==2)
			out[k+2] = b64_chr[ ((s[1]&0x0F)<<2) ];
		else
			out[k+2] = '=';
		out[k+3] = '=';
		k+=4;
	}
	out[k] = '\0';
	return k;
}





unsigned int b64_decode(const unsigned char* in, unsigned int in_len, unsigned char* out) {

	unsigned int i=0, j=0, k=0, s[4];
	
	for (i=0;i<in_len;i++) {
		s[j++]=b64_int(*(in+i));
		if (j==4) {
			out[k+0] = ((s[0]&255)<<2)+((s[1]&0x30)>>4);
			if (s[2]!=64) {
				out[k+1] = ((s[1]&0x0F)<<4)+((s[2]&0x3C)>>2);
				if ((s[3]!=64)) {
					out[k+2] = ((s[2]&0x03)<<6)+(s[3]); k+=3;
				} else {
					k+=2;
				}
			} else {
				k+=1;
			}
			j=0;
		}
	}
	
	return k;
}

unsigned int b64_encodef(char *InFile, char *OutFile) {

	FILE *pInFile = fopen(InFile,"rb");
	FILE *pOutFile = fopen(OutFile,"wb");
	
	unsigned int i=0;
	unsigned int j=0;
	unsigned int c=0;
	unsigned int s[4];
	
	if ((pInFile==NULL) || (pOutFile==NULL) ) {
		if (pInFile!=NULL){fclose(pInFile);}
		if (pOutFile!=NULL){fclose(pOutFile);}
		return 0;
	}
	
	while(c!=EOF) {
		c=fgetc(pInFile);
		if (c==EOF)
		break;
		s[j++]=c;
		if (j==3) {
			fputc(b64_chr[ (s[0]&255)>>2 ],pOutFile);
			fputc(b64_chr[ ((s[0]&0x03)<<4)+((s[1]&0xF0)>>4) ],pOutFile);
			fputc(b64_chr[ ((s[1]&0x0F)<<2)+((s[2]&0xC0)>>6) ],pOutFile);
			fputc(b64_chr[ s[2]&0x3F ],pOutFile);
			j=0; i+=4;
		}
	}
	
	if (j) {
		if (j==1)
			s[1] = 0;
		fputc(b64_chr[ (s[0]&255)>>2 ],pOutFile);
		fputc(b64_chr[ ((s[0]&0x03)<<4)+((s[1]&0xF0)>>4) ],pOutFile);
		if (j==2)
			fputc(b64_chr[ ((s[1]&0x0F)<<2) ],pOutFile);
		else
			fputc('=',pOutFile);
		fputc('=',pOutFile);
		i+=4;
	}
	
	fclose(pInFile);
	fclose(pOutFile);
	
	return i;
}







unsigned int b64_decodef(char *InFile, char *OutFile) 
{

	FILE *pInFile = fopen(InFile,"rb");
	FILE *pOutFile = fopen(OutFile,"wb");
	
	unsigned int c=0;
	unsigned int j=0;
	unsigned int k=0;
	unsigned int s[4];
	
	if ((pInFile==NULL) || (pOutFile==NULL) ) {
		if (pInFile!=NULL){fclose(pInFile);}
		if (pOutFile!=NULL){fclose(pOutFile);}
		return 0;
	}
	
	while(c!=EOF) {
		c=fgetc(pInFile);
		if (c==EOF)
		break;
		s[j++]=b64_int(c);
		if (j==4) {
			fputc(((s[0]&255)<<2)+((s[1]&0x30)>>4),pOutFile);
			if (s[2]!=64) {
				fputc(((s[1]&0x0F)<<4)+((s[2]&0x3C)>>2),pOutFile);
				if ((s[3]!=64)) {
					fputc(((s[2]&0x03)<<6)+(s[3]),pOutFile); k+=3;
				} else {
					k+=2;
				}
			} else {
				k+=1;
			}
			j=0;
		}
	}

	fclose(pInFile);
	fclose(pOutFile);
	
	return k;
}






int cutelower(int a) 
{   //https://stackoverflow.com/a/15709023/883015
    if ((a >= 0x41) && (a <= 0x5A))
        a |= 0x20; 
    return a;  
}






void encode_str_print( char *myfoostr )
{
	int bcoded = 0;
	int  tlen = strlen( myfoostr );
	char *b64 = (char *)malloc(1 + (sizeof(char) * b64e_size(tlen)));
	bcoded = b64_encode( myfoostr , tlen, b64 );
	printf("%s\n", b64);
	free(b64);
}







void decode_str_print( char *myfoostr )
{
	int bcoded = 0;
	int blen = strlen( myfoostr );
	char *txt = malloc(1 + (sizeof(char) * b64d_size(blen)));
	bcoded = b64_decode( myfoostr ,blen,txt);
	txt[bcoded] = '\0';
	printf("%s\n", txt);
	free(txt);
}




void pipe_lc()
{
	        // the little line count
	        double mycount = 0;
		int begin = 1;
		float charpos = 1 ; 
		int c ; 
		c = getchar();
		while( c != EOF )
		{
			begin = 0;
			if ( c == '\n' ) 
			{ begin = 1; mycount++; }
			c = getchar();
		}
		printf( "Count: %g\n" , mycount );
}






void encode_pipe( )
{
        int begin = 1;
        char linebuffer[BUFSIZ];
        strncpy( linebuffer, "" , BUFSIZ );
        char ptr[BUFSIZ];
        int j=0;
        int c ; 
        c = getchar();
        while( c != EOF )
        {
            if ( c == '\n' )
            {  
               begin = 1;
               ptr[j]='\0';
               encode_str_print( ptr );
               strncpy( ptr, "", BUFSIZ );
               j = 0;
            }
            else
            {
              ptr[j++] = c;
              begin = 0;
            }
            c = getchar();
        }
}






void decode_pipe( )
{
        int begin = 1;
        char linebuffer[BUFSIZ];
        strncpy( linebuffer, "" , BUFSIZ );
        char ptr[BUFSIZ];
        int j=0;
        int c ; 
        c = getchar();
        while( c != EOF )
        {
            if ( c == '\n' )
            {  
               begin = 1;
               ptr[j]='\0';
               decode_str_print( ptr );
               strncpy( ptr, "", BUFSIZ );
               j = 0;
            }
            else
            {
              ptr[j++] = c;
              begin = 0;
            }
            c = getchar();
        }
}








int main(int argc,char** argv) 
{
	int bcoded = 0;

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "--pipe-lc" ) ==  0 ) 
	{
	        pipe_lc(); 
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "--enc" ) ==  0 ) 
	{
	        encode_pipe(); 
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "--dec" ) ==  0 ) 
	{
	        decode_pipe(); 
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "--arg-enc" ) ==  0 ) 
	{
		encode_str_print( argv[2] ); 
		return 0;
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "--arg-dec" ) ==  0 ) 
	{
		decode_str_print( argv[2] ); 
		return 0;
	}


	if ( argc == 4 )
	if ( strcmp( argv[1] ,   "--dec-file" ) ==  0 ) 
	{
	        puts("\nDECODING");
	        printf("SOURCE: %s\n", argv[ 2 ] );
	        printf("TARGET: %s\n", argv[ 3 ] );
        	b64_decodef( argv[2] , argv[3] );
		return 0;
	}


	if ( argc == 4 )
	if ( strcmp( argv[1] ,   "--enc-file" ) ==  0 ) 
	{
	        puts("\nENCODING");
	        printf("SOURCE: %s\n", argv[ 2 ] );
	        printf("TARGET: %s\n", argv[ 3 ] );
        	b64_encodef( argv[2] , argv[3] );
		return 0;
	}



	char opt = ' ';
	if (argc > 1)
		opt = cutelower(argv[1][0]);
	

	if ( ((argc < 4) && (opt!='b' && opt!='t')) ) 
	{
		puts("------------------------------------\n");
		puts("     H  E  L P \n "); 
		puts("------------------------------------\n");
		puts("\nbase64.c [Encode/Decode]");
		puts("------------------------------------");
		printf("File mode\n");
		printf("\tUse the following to encode:\n\t%s e(ncode) IN_filepath OUT_filepath\n",argv[0]);
		printf("\tUse the following to decode:\n\t%s d(ecode) IN_filepath OUT_filepath\n",argv[0]);
		printf("\nText mode (outputs to stdout):\n");
		printf("\tUse the following to encode:\n\t%s t(ext) IN_Text\n",argv[0]);
		printf("\tUse the following to decode:\n\t%s b(ase64) IN_Base64\n",argv[0]);
		///puts("\nERROR: insufficient or incorrect parameters...");
		///puts(   "------------------------------------");
		///printf( " HELP: cbase64 --enc-file test.txt test.enc \n" ); 
		puts(   "------------------------------------");
		printf(  " HELP TO ENCODE AND DECODE:\n" ); 
                printf(  "     ls -1 > test.txt ; tcbase64 --enc-file test.txt test.enc \n" ); 
                printf(  "     tcbase64 --dec-file test.enc test.txt2\n" ); 
		puts(   "------------------------------------");
		printf(  " HELP TO ENCODE AND DECODE:\n" ); 
		printf( "   tcbase64  --arg-enc  'hello world' \n" ); 
		printf( "   Encoded base64 from text: aGVsbG8gd29ybGQ=\n" ); 
		printf( "   tcbase64  --arg-dec  'aGVsbG8gd29ybGQ=' \n" ); 
		printf( "   Decoded text from base64: hello world\n" ); 
		puts(   "------------------------------------");
		printf(  " HELP TO ENCODE AND DECODE:\n" ); 
		printf( "   echo test | tcbase64 --enc  \n" ); 
		printf( "   echo  aGVsbG8gd29ybGQ=  | tcbase64 --dec  \n" ); 
		puts(   "------------------------------------");

		return 1;
	}
	





	switch(opt) {
		case 'd':
			puts("\nDECODING");
			bcoded = b64_decodef(argv[2],argv[3]);
			break;
		case 'e':
			puts("\nENCODING");
			bcoded = b64_encodef(argv[2],argv[3]);
			break;

		case 't':
			puts("\nENCODING from text to base64");
			
				int tlen = strlen( argv[2] );
				char *b64 = (char *)malloc(1 + (sizeof(char) * b64e_size(tlen)));
				if ( b64 == NULL ) 
				{
					printf("Error: cannot allocate memory for output\n");
					exit(1);  /* End program with error status. */
				}
			
			bcoded = b64_encode(argv[2],tlen,b64);
			printf("Encoded base64 from text: %s", b64);
			free(b64);
			break;

		case 'b':
			puts("\nDECODING from base64 to text");
			int blen = strlen(argv[2]);
			char *txt = malloc(1 + (sizeof(char) * b64d_size(blen)));
			if (txt == NULL) {
					printf("Error: cannot allocate memory for output\n");
					exit(1);  /* End program with error status. */
			}
			bcoded = b64_decode(argv[2],blen,txt);
			txt[bcoded] = '\0';
			printf("Decoded text from base64: %s", txt);
			free(txt);
			break;

		default:
			puts("\nINVALID OPTION");
			bcoded = -1;
	}
	
	printf("\nBytes encoded/decoded: %i\n",bcoded);
	
	return 0;
}




